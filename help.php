<?php
include_once('./config.php');

include_once('./includes/pageHeader.php');
include_once(DOCROOT . 'includes/pageFunctions.php');

show_banner();
echo "<fieldset class='output output2'>";
echo "<legend>FAQ</legend>";

$fh = fopen('./faqs.txt', 'r');
//read file in a line at a time, split on :
while($line = fgets($fh))
{
    echo "<p>$line</p>";
    if(substr($line, 0, 1) === 'A')
    {
        //horizontal rule between q/as
        echo "<hr />";
    }
}

echo "</fieldset>";

echo "<fieldset class='output output2'>";
echo "<legend>Schema</legend>";

$mysqli = getMysqliAPPConnector();

//$tableArray = array('students');

//var_dump($mysqli);
foreach ($SCHEMAS_TO_SEE as $table) {
    $query = "DESCRIBE $table";
    $result = $mysqli->query($query) or die('cannot do query');
    echo "<h3>$table</h3>";
    echo "<table>";
    while ($row = $result->fetch_row()) {
        echo "<tr>";
        foreach ($row as $val) {
            echo "<td>$val</td>";
        }
        echo "</tr>";
    }
    echo "</table>";
}


echo "</fieldset>";
include_once('./includes/pageFooter.php');

?>
