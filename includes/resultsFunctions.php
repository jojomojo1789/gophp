<?php
/*
 * updated 9-11-2013
 * To receive an argument for a particular type of question
 */
include_once './config.php';
include_once(DOCROOT . 'includes/pageFunctions.php');

function getStudentScores() {
    $s = '';

    $mysqli = new mysqli('p:' . DB_HOST, DB_USER, DB_PASSWORD, DB_DATABASE);

    $table = DB_TABLE_PREFIX . 'person_question';
    $stmt = $mysqli->query("SELECT * FROM $table");


    while ($row = mysqli_fetch_row($stmt)) {
        //var_dump($row);
        $s .= "<tr>";
        $s .= "<td>$row[0]</td>";
        $s .= "<td>$row[1]</td>";
        $s .= "<td>$row[2]</td>";
        $s .= "<td>$row[3]</td>";

        $img = ($row[4] == 1) ? IMG_CORRECT : IMG_NOTCORRECT;
        $s .= "<td><img src=$img class='icon'/></td>";


        //link to view students code.
        $s .= "<td><a href='./page2.php?studentID=$row[0]&qID=$row[1]'>view</a></td>";

        $s.="</tr>";
    }

    return $s;
}

function show_student_list() {
    ?>
    <form id="filter-form">Filter: <input name="filter" id="filter" value="" maxlength="30" size="30" type="text"></form><br>
    <table id="myTable" class="tablesorter">
        <thead>
            <tr>
                <th>StudentID</th><th>Question ID</th><th>Number attempts</th><th>Last modified</th><th>Correct</th><th>Code</th>
            </tr>
        </thead>
        <tbody>
            <?= getStudentScores() ?>
        </tbody>
    </table>

    <?
}

function solved_all($qType = 0) {
    $mysqli = new mysqli('p:' . DB_HOST, DB_USER, DB_PASSWORD, DB_DATABASE);

    $table = DB_TABLE_PREFIX . 'questions';
    $stmt = $mysqli->query("SELECT count(*) FROM $table");


    $row = mysqli_fetch_row($stmt);
    $question_count = $row[0];

    //add WHERE statemen
    $where = '';
    if ($qType != 0) {
        $where = "WHERE q.question_type=$qType ";
    }

    //once we have the number of questions, see h
    $qry = "SELECT pq.student_id, p.fname, p.lname, count(pq.correct) as cor 
        FROM " . DB_TABLE_PREFIX . 'person as p 
	JOIN ' . DB_TABLE_PREFIX . 'questions as q
        JOIN ' . DB_TABLE_PREFIX . "person_question as pq 
	ON q.question_id = pq.question_id
	AND pq.student_id=p.username 
	$where 
	GROUP BY pq.student_id";
    //question_type set to 3 for output questions.
    $stmt = $mysqli->query($qry);
    //echo $qry;

    while ($row = mysqli_fetch_assoc($stmt)) {
        if ($qType == 0) {
            if ($row['cor'] == $question_count) {
                echo "<p>$row[fname] $row[lname] $row[cor]</p>";
            }
        } else {
            echo "<p>$row[fname] $row[lname] $row[cor]</p>";
        }
    }
}
?>
