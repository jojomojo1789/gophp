<?php
/*
 * On this page the admin should be able to login and see the results of each student
 */



include_once 'includes/resultsFunctions.php';
include_once 'includes/pageHeader.php';
//if(!isset($_SESSION))
//{
//    session_name("MAIN");
//    session_start();
//}
//$p = new Person($_SESSION['username']);
show_banner();
?>
<div id="tabs" class="output">
    <ul>
        <li><a href="#tabs-1">Solved</a></li>
        <li><a href="#tabs-2">Who solved all?</a></li>
        <li><a href="#tabs-3">Reset</a></li>
    </ul>
    <div id="tabs-1">
        <? show_student_list(); ?>
    </div>
    <div id="tabs-2">
        <?
        //create dropdown for problem types here
        //after they select, call solved_all for ajax
        echo "<div>";
        echo "<form id=frm_results>";
        echo getAllQuestionTypesAsSelect();
        echo "</form>";
        echo "</div>";
        echo "<div id=solved>";
        echo "</div>";
        solved_all();
        ?>

    </div>
    <div id="tabs-3">
        <p>Reset- need to implement function here to reset the db.</p>
    </div>
</div>

<?
include_once 'includes/pageFooter.php';
?>
