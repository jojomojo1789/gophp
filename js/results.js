/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function() {
    $("#myTable").tablesorter();

    $(function() {
        var theTable = $('#myTable');

//        theTable.find("tbody > tr").find("td:eq(1)").mousedown(function(){
//            $(this).prev().find(":checkbox").click()
//        });

        $("#filter").keyup(function() {
            $.uiTableFilter(theTable, this.value);
        })

        $('#filter-form').submit(function() {
            theTable.find("tbody > tr:visible > td:eq(1)").mousedown();
            return false;
        }).focus(); //Give focus to input field
    });

    //when selecting different question types
    //we want to display the results
    
    //TODO: make it show it displays who has solved all when the page first loads.
    //JUST need to send a questionType of 0 to the ajax page.
    $(function() {
        $('#frm_results').change(function(e) {
            $.post('./ajaxResults.php?js=1', {
                questionType: document.getElementById('select_type').value,
            }, function(res) {
                //alert(res);
                $('#solved').html(res);
            });
        });
    });

});
